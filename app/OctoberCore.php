<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OctoberCore extends Model
{
    protected $table = 'October2021';
    public $timestamps = false;
    protected $primaryKey = 'CSHRV_Account';
    public $incrementing = false;
}

