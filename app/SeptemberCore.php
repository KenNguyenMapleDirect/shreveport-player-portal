<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SeptemberCore extends Model
{
    protected $table = 'September2021';
    public $timestamps = false;
    protected $primaryKey = 'CSHRV_Account';
    public $incrementing = false;
}

