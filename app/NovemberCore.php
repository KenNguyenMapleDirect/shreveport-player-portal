<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class NovemberCore extends Model
{
    protected $table = 'November2021';
    public $timestamps = false;
    protected $primaryKey = 'CSHRV_Account';
    public $incrementing = false;
}

