<?php

namespace App\Http\Controllers\Voyager;

use App\Data;
use App\Flipbook;
use App\JuneCoreSM;
use App\JulyCorePC;
use App\AugustCore;
use App\SeptemberCore;
use App\OctoberCore;
use App\NovemberCore;
use App\User;
use App\WeekenderFlipbook;
use App\MayCoreSM;
use App\MayCorePC;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;

class VoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerController
{
    public function makeImageForSeptemberCorePC($accountId, $imageTextData, $fileName)
    {
        $img = Image::make('https://s3.amazonaws.com/shreveport.maplewebservices.com/september_2021/hi_res/'.$fileName.'.jpg');
        $img->text($imageTextData, 180, 270, function ($font) {
            $font->file(public_path('PostCardImage/SeptemberCorePC/fonts/steelfish rg.ttf'));
            $font->size(120);
            $font->color('#000');
            $font->align('left');
            $font->angle(0);
        });
        if (!File::exists('PostCardImage/SeptemberCorePC/ImageByAccountId/' . $accountId . '.jpg'))
        {
            $img->save(public_path('PostCardImage/SeptemberCorePC/ImageByAccountId/' . $accountId . '.jpg'));
        }
    }

    //Function Get data by Account Id
    public function getDataByAccountId($accountId)
    {
        //define data return
        $data = new \stdClass();
        $data->first_name = '';
        $data->last_name = '';
        $data->CSHRV_Account = $accountId;
        $data->CSHRV_Tier = '';
        $data->CSHRV_MTD_Points = 0;
        $data->CSHRV_Points = 0;
        $data->CSHRV_Comp_Dollars = 0;
        $data->CSHRV_Points_Next_Tier = 0;
        $data->CSHRV_Poker_Rating = 0;
        $data->updated_at = now();

        //Flag to check if have SM or PC data
        $data->flgSM = 0;
        $data->flgPC = 0;

        //Get data from Datas table
        $datas = Data::where('CSHRV_Player_ID', $accountId)->first();
        if ($datas) {
            $data->first_name = $datas->CSHRV_FName;
            $data->last_name = $datas->CSHRV_LName;
            $data->CSHRV_Tier = $datas->CSHRV_Tier;
            $data->CSHRV_MTD_Points = $datas->CSHRV_MTD_Points;
            if($datas->CSHRV_Points !== '')
            $data->CSHRV_Points = $datas->CSHRV_Points;
            $data->CSHRV_Comp_Dollars = $datas->CSHRV_Comp_Dollars;
            $data->CSHRV_Points_Next_Tier = $datas->CSHRV_Points_Next_Tier;
            $data->CSHRV_Poker_Rating = $datas->CSHRV_Poker_Rating;
        }

         //Get data from November2021 table
        //Define data type flag

        $data->NovemberSundayGiftPC = 0;
        $data->NovemberSundayGiftPCLabel = '';
        $data->NovemberHEGiftPC = 0;
        $data->NovemberHEGiftPCLabel = '';
        $data->NovemberCoreSM = 0;
        $data->NovemberCoreSMLabel = '';
        $data->NovemberCorePC = 0;
        $data->NovemberCorePCLabel = '';
        $data->NovemberHighLimitPC = 0;
        $data->NovemberHighLimitPCLabel = '';


        $dataFromNovember2021 = NovemberCore::where('CSHRV_Account', $accountId)->get();
        if ($dataFromNovember2021) {
            foreach ($dataFromNovember2021 as $singleDataFromNovember2021) {
                //CoreSM
                if ($singleDataFromNovember2021->CSHRV_Mailer_Type === "Core SM") {
                    $data->NovemberCoreSM = 1;
                    $data->NovemberCoreSMLabel = $singleDataFromNovember2021->CSHRV_Label;
                    $data->flgSM =1;

                    $data->NovemberCoreSMResult1 = $singleDataFromNovember2021->CSHRV_Img_Page01 . ".jpg";
                    $data->NovemberCoreSMResult2 = $singleDataFromNovember2021->CSHRV_Img_Page02 . ".jpg";
                    $data->NovemberCoreSMResult3 = $singleDataFromNovember2021->CSHRV_Img_Page03 . ".jpg";
                    $data->NovemberCoreSMResult4 = $singleDataFromNovember2021->CSHRV_Img_Page04 . ".jpg";
                    $data->NovemberCoreSMResult5 = $singleDataFromNovember2021->CSHRV_Img_Page05 . ".jpg";
                    $data->NovemberCoreSMResult6 = $singleDataFromNovember2021->CSHRV_Img_Page06 . ".jpg";
                    $data->NovemberCoreSMResult7 = $singleDataFromNovember2021->CSHRV_Img_Page07 . ".jpg";
                    $data->NovemberCoreSMResult8 = $singleDataFromNovember2021->CSHRV_Img_Page08 . ".jpg";
                }

                //CorePC and other
                if ($singleDataFromNovember2021->CSHRV_Mailer_Type === "Core PC") {
                    $data->NovemberCorePC = 1;
                    $data->NovemberCorePCLabel = $singleDataFromNovember2021->CSHRV_Label;
                    $data->flgPC =1;

                    $data->NovemberCorePCResult1 = $singleDataFromNovember2021->CSHRV_Img_Page01 . ".jpg";
                    $data->NovemberCorePCResult2 = $singleDataFromNovember2021->CSHRV_Img_Page02 . ".jpg";
                }

                if ($singleDataFromNovember2021->CSHRV_Mailer_Type === "SUNDAY_GIFT") {
                    $data->NovemberSundayGiftPC = 1;
                    $data->NovemberSundayGiftPCLabel = $singleDataFromNovember2021->CSHRV_Label;
                    $data->flgPC =1;
                    $data->NovemberSundayGiftPCResult1 = $singleDataFromNovember2021->CSHRV_Img_Page01 . ".jpg";
                    $data->NovemberSundayGiftPCResult2 = $singleDataFromNovember2021->CSHRV_Img_Page02 . ".jpg";
                }
                if ($singleDataFromNovember2021->CSHRV_Mailer_Type === "High End PC") {
                    $data->NovemberHEGiftPC = 1;
                    $data->NovemberHEGiftPCLabel = $singleDataFromNovember2021->CSHRV_Label;
                    $data->flgPC =1;

                }
                if ($singleDataFromNovember2021->CSHRV_Mailer_Type === "HIGH_LIMIT_PC") {
                    $data->NovemberHighLimitPC = 1;
                    $data->NovemberHighLimitPCLabel = $singleDataFromNovember2021->CSHRV_Label;
                    $data->flgPC =1;

                }

            }
        }
        return $data;
    }

    //get data for User Account
    public function index()
    {
        if (Auth::user()->role_id === 1 || Auth::user()->role_id === 4)
            return Voyager::view('voyager::index');
        else {
            //get user data and get data
            $data = $this->getDataByAccountId(Auth::user()->CSHRV_Player_ID);
            return view('player-dashboard')->with('data', $data);
        }
    }

    //get data for SuperUser Account
    public function getViewPlayerDashBoardByAccountId($accountId)
    {
        //get user data and get data
        $data = $this->getDataByAccountId($accountId);

        return view('player-dashboard')->with('data', $data);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('voyager.login');
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string)$image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return "<script> parent.helpers.setImageValue('" . Voyager::image($fullFilename) . "'); </script>";
    }
}
