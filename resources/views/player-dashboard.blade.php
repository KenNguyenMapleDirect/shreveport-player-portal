@extends('layouts.app')
@section('content')
    <div class="row justify-content-center" style="padding-top: 20px;background: white">
        <div class="col-md-9">
        @if($data->flgSM || $data->flgPC)
            <!-- SM here -->
                <div class="row">
                    @if($data->NovemberCoreSM)
                        <div class="col-md-12">
                            <script type="text/javascript"
                                    src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                            <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                            <script type="text/javascript"
                                    src="{{asset('flipbook_assets/js/lightbox.min.js')}}"></script>
                            <style>
                                table {
                                    border-collapse: inherit !important;
                                }

                                #img-magnifier-container {
                                    display: none;
                                    background: rgba(0, 0, 0, 0.8);
                                    border: 5px solid rgb(255, 255, 255);
                                    border-radius: 20px;
                                    box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                    0 0 7px 7px rgba(0, 0, 0, 0.25),
                                    inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                    cursor: none;
                                    position: absolute;
                                    pointer-events: none;
                                    width: 400px;
                                    height: 200px;
                                    overflow: hidden;
                                    z-index: 999;
                                }

                                .glass {
                                    position: absolute;
                                    background-repeat: no-repeat;
                                    background-size: auto;
                                    cursor: none;
                                    z-index: 1000;
                                }

                                #toggle-zoom {
                                    background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                    background-size: 40px;
                                    display: block;
                                    width: 40px;
                                    display: none;
                                    height: 40px;
                                }

                                #printer {
                                    float: right;
                                    display: block;
                                    width: 40px;
                                    height: 40px;
                                    margin-right: 20px;
                                    display: none;
                                }

                                #toggle-zoom.toggle-on {
                                    background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                }

                                @media (hover: none) {
                                    .tool-zoom {
                                        display: none;
                                    }

                                    #printer {
                                        display: none;
                                    }
                                }
                            </style>
                            <div class="flipbook-viewport">
                                <div class="container">
                                    <div>
                                        <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}""
                                           target="blank"><img
                                                id="printer"
                                                src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                    </div>
                                    <div class="tool-zoom">
                                        <a id="toggle-zoom" onclick="toggleZoom()"></a>
                                    </div>

                                    <div class="arrows">
                                        <div class="arrow-prev">
                                            <a id="prev"><img class="previous" width="20"
                                                              src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                              alt=""/></a>
                                        </div>
                                        <center><h4 style="color: #EB1D32;">{{$data->NovemberCoreSMLabel}}</h4>
                                        </center>
                                        <div class="flipbook">

                                            <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult1}}"
                                               data-odd="1"
                                               id="page-1"
                                               data-lightbox="big" class="page"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult1}}')"></a>

                                            <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult2}}"
                                               data-even="2"
                                               id="page-2" data-lightbox="big"
                                               class="single"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult2}}')"></a>

                                            <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult3}}"
                                               data-odd="3"
                                               id="page-3"
                                               data-lightbox="big" class="single"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult3}}')"></a>

                                            <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult4}}"
                                               data-even="4"
                                               id="page-4" data-lightbox="big"
                                               class="single"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult4}}')"></a>

                                            <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult5}}"
                                               data-even="5"
                                               id="page-5" data-lightbox="big"
                                               class="single"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult5}}')"></a>

                                            <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult6}}"
                                               data-even="6"
                                               id="page-6" data-lightbox="big"
                                               class="single"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult6}}')"></a>
                                               <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult7}}"
                                               data-even="7"
                                               id="page-7" data-lightbox="big"
                                               class="single"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult7}}')"></a>
                                               <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCoreSMResult8}}"
                                               data-even="8"
                                               id="page-8" data-lightbox="big"
                                               class="single"
                                               style="background-image: url('https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult8}}')"></a>
                                        </div>
                                        <div class="arrow-next">
                                            <a id="next"><img class="next" width="20"
                                                              src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                              alt=""/></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flipbook-slider-thumb">
                                <div class="drag">
                                    <!-- <img id="prev-arrow" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                                    <img onclick="onPageClick(1)"
                                         class="thumb-img left-img"
                                         src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult1}}"
                                         alt=""/>

                                    <div class="space">
                                        <img onclick="onPageClick(2)" class="thumb-img"
                                             src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult2}}"
                                             alt=""/>
                                        <img onclick="onPageClick(3)" class="thumb-img"
                                             src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult3}}"
                                             alt=""/>
                                    </div>

                                    <div class="space">
                                        <img onclick="onPageClick(4)" class="thumb-img"
                                             src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult4}}"
                                             alt=""/>
                                        <img onclick="onPageClick(5)" class="thumb-img"
                                             src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult5}}"
                                             alt=""/>
                                    </div>

                                    <div class="space">
                                        <img onclick="onPageClick(6)"
                                             class="thumb-img active"
                                             src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult6}}"
                                             alt=""/>
                                    </div>
                                    <div class="space">
                                        <img onclick="onPageClick(7)" class="thumb-img"
                                             src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult7}}"
                                             alt=""/>
                                        <img onclick="onPageClick(8)" class="thumb-img"
                                             src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCoreSMResult8}}"
                                             alt=""/>
                                    </div>

                                <!-- <img id="next-arrow" class="thumb-arrow" src="{{asset('flipbook_assets/pages/right-arrow.svg')}}" alt=""> -->
                                </div>

                                <ul class="flipbook-slick-dots" role="tablist">
                                    <li onclick="onPageClick(1)" class="dot">
                                        <a type="button" style="color: #7f7f7f">1</a>
                                    </li>
                                    <li onclick="onPageClick(2)" class="dot">
                                        <a type="button" style="color: #7f7f7f">2</a>
                                    </li>
                                    <li onclick="onPageClick(3)" class="dot">
                                        <a type="button" style="color: #7f7f7f">3</a>
                                    </li>
                                    <li onclick="onPageClick(4)" class="dot">
                                        <a type="button" style="color: #7f7f7f">4</a>
                                    </li>
                                    <li onclick="onPageClick(5)" class="dot">
                                        <a type="button" style="color: #7f7f7f">5</a>
                                    </li>
                                    <li onclick="onPageClick(6)" class="dot">
                                        <a type="button" style="color: #7f7f7f">6</a>
                                    </li>
                                    <li onclick="onPageClick(7)" class="dot">
                                        <a type="button" style="color: #7f7f7f">7</a>
                                    </li>
                                    <li onclick="onPageClick(8)" class="dot">
                                        <a type="button" style="color: #7f7f7f">8</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="img-magnifier-container">
                                <img id="zoomed-image-container" class="glass" src=""/>
                            </div>
                            <div id="log"></div>
                            <audio id="audio" style="display: none"
                                   src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                            <script type="text/javascript">
                                function scaleFlipBook() {
                                    var imageWidth = 541;
                                    var imageHeight = 850;
                                    var pageHeight = 550;
                                    var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                                    $(".flipbook-viewport .container").css({
                                        width: 40 + pageWidth * 2 + 40 + "px",
                                    });

                                    $(".flipbook-viewport .flipbook").css({
                                        width: pageWidth * 2 + "px",
                                        height: pageHeight + "px",
                                    });

                                    $(".flipbook-viewport .flipbook").css('margin-bottom', 20);
                                }

                                function doResize() {
                                    $("html").css({
                                        zoom: 1
                                    });
                                    var $viewport = $(".flipbook-viewport");
                                    var viewHeight = $viewport.height();
                                    var viewWidth = $viewport.width();

                                    var $el = $(".flipbook-viewport .container");
                                    var elHeight = $el.outerHeight();
                                    var elWidth = $el.outerWidth();

                                    var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                    //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                    if (scale < 1) {
                                        scale *= 0.95;
                                    } else {
                                        scale = 1;
                                    }
                                    // $("html").css({
                                    //     zoom: scale
                                    // });
                                }

                                function loadApp() {
                                    scaleFlipBook();
                                    var flipbook = $(".flipbook");

                                    // Check if the CSS was already loaded

                                    if (flipbook.width() == 0 || flipbook.height() == 0) {
                                        setTimeout(loadApp, 10);
                                        return;
                                    }

                                    $(".flipbook .double").scissor();

                                    // Create the flipbook

                                    $(".flipbook").turn({
                                        // Elevation

                                        elevation: 50,

                                        // Enable gradients

                                        gradients: true,

                                        // Auto center this flipbook

                                        autoCenter: true,
                                        when: {
                                            turning: function (event, page, view) {
                                                var audio = document.getElementById("audio");
                                                audio.play();
                                            },
                                            turned: function (e, page) {
                                                //console.log('Current view: ', $(this).turn('view'));
                                                var thumbs = document.getElementsByClassName("thumb-img");
                                                for (var i = 0; i < thumbs.length; i++) {
                                                    var element = thumbs[i];
                                                    if (element.className.indexOf("active") !== -1) {
                                                        $(element).removeClass("active");
                                                    }
                                                }

                                                $(
                                                    document.getElementsByClassName("thumb-img")[page - 1]
                                                ).addClass("active");

                                                var dots = document.getElementsByClassName("dot");
                                                for (var i = 0; i < dots.length; i++) {
                                                    var dot = dots[i];
                                                    if (dot.className.indexOf("dot-active") !== -1) {
                                                        $(dot).removeClass("dot-active");
                                                    }
                                                }
                                            },
                                        },
                                    });
                                    doResize();
                                }

                                $(window).resize(function () {
                                    doResize();
                                });
                                $(window).bind("keydown", function (e) {
                                    if (e.keyCode == 37) $(".flipbook").turn("previous");
                                    else if (e.keyCode == 39) $(".flipbook").turn("next");
                                });
                                $("#prev").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("previous");
                                });

                                $("#next").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("next");
                                });

                                $("#prev-arrow").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("previous");
                                });

                                $("#next-arrow").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("next");
                                });

                                function onPageClick(i) {
                                    $(".flipbook").turn("page", i);
                                }

                                // Load the HTML4 version if there's not CSS transform
                                yepnope({
                                    test: Modernizr.csstransforms,
                                    yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                    nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                    both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page.css')}}"],
                                    complete: loadApp,
                                });

                                zoomToolEnabled = false;

                                function toggleZoom() {
                                    if (zoomToolEnabled) {
                                        $(".flipbook a").off("mousemove");
                                        $("#toggle-zoom").removeClass("toggle-on");
                                        $("#img-magnifier-container").hide();

                                        zoomToolEnabled = false;
                                    } else {
                                        $(".flipbook a").mousemove(function (event) {
                                            var magnifier = $("#img-magnifier-container");
                                            $("#img-magnifier-container").css(
                                                "left",
                                                event.pageX - magnifier.width() / 2
                                            );
                                            $("#img-magnifier-container").css(
                                                "top",
                                                event.pageY - magnifier.height() / 2
                                            );
                                            $("#img-magnifier-container").show();
                                            var hoveredImage = $(event.target).css("background-image");
                                            var bg = hoveredImage
                                                .replace("url(", "")
                                                .replace(")", "")
                                                .replace(/\"/gi, "");
                                            // Find relative position of cursor in image.
                                            var targetPage = $(event.target);
                                            var targetLeft = 400 / 2; // Width of glass container/2
                                            var targetTop = 200 / 2; // Height of glass container/2

                                            var zoomedImageContainer = document.getElementById(
                                                "zoomed-image-container"
                                            );
                                            var zoomedImageWidth = zoomedImageContainer.width;
                                            var zoomedImageHeight = zoomedImageContainer.height;

                                            var imgXPercent =
                                                (event.pageX - $(event.target).offset().left) /
                                                targetPage.width();
                                            targetLeft -= zoomedImageWidth * imgXPercent;
                                            var imgYPercent =
                                                (event.pageY - $(event.target).offset().top) /
                                                targetPage.height();
                                            targetTop -= zoomedImageHeight * imgYPercent;

                                            $("#img-magnifier-container .glass").attr("src", bg);
                                            $("#img-magnifier-container .glass").css(
                                                "top",
                                                "" + targetTop + "px"
                                            );
                                            $("#img-magnifier-container .glass").css(
                                                "left",
                                                "" + targetLeft + "px"
                                            );
                                        });

                                        $("#toggle-zoom").addClass("toggle-on");
                                        zoomToolEnabled = true;
                                    }
                                }
                            </script>
                        </div>
                    @endif
                </div>
                <!-- PC here -->
                @if($data->NovemberCorePC)
                        <div class="col-sm-12">
                            <div class="modal-wrap">
                                <h4 style="color: #EB1D32;text-align: center;">{{$data->NovemberCorePCLabel}}</h4>

                                <div class="slider-big">
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCorePCResult1}}"
                                       data-lightbox-fifth="roadtrip">
                                        <center><img
                                                src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCorePCResult1}}"
                                                style="width: 50%" alt=""></center>
                                    </a>
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberCorePCResult2}}"
                                       data-lightbox-fifth="roadtrip">
                                        <img
                                            src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberCorePCResult2}}"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>

                            <script src="/weekender_assets/js/modal_lightbox_fifth.min.js"></script>
                        </div>
                    @endif
                <br>
                <div class="row">
                    @if($data->NovemberSundayGiftPC)
                        <div class="col-sm-4">
                            <div class="modal-wrap">
                                <h4 style="color: #EB1D32;text-align: center;">{{$data->NovemberSundayGiftPCLabel}}</h4>

                                <div class="slider-big">
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/october_2021/hi_res/133276_NOV2021_SUNDAY_GIFT_PC_P01.jpg"
                                       data-lightbox-fourth="roadtrip">
                                        <center><img
                                                src="https://s3.amazonaws.com/shreveport.maplewebservices.com/october_2021/lo_res/133276_NOV2021_SUNDAY_GIFT_PC_P01.jpg"
                                                style="width: 20%" alt=""></center>
                                    </a>
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/october_2021/hi_res/133276_NOV2021_SUNDAY_GIFT_PC_P02.jpg"
                                       data-lightbox-fourth="roadtrip">
                                        <img
                                            src="https://s3.amazonaws.com/shreveport.maplewebservices.com/october_2021/lo_res/133276_NOV2021_SUNDAY_GIFT_PC_P02.jpg"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>

                            <script src="/weekender_assets/js/modal_lightbox_fourth.min.js"></script>
                        </div>
                    @endif
                    @if($data->NovemberHEGiftPC)
                        <div class="col-sm-4">
                            <div class="modal-wrap">
                                <h4 style="color: #EB1D32;text-align: center;">{{$data->NovemberHEGiftPCLabel}}</h4>

                                <div class="slider-big">
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/133277_NOV2021_HIGHEND_GIFT_PC_P01.jpg"
                                       data-lightbox-fifth="roadtrip">
                                        <center><img
                                                src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/133277_NOV2021_HIGHEND_GIFT_PC_P01.jpg"
                                                style="width: 20%" alt=""></center>
                                    </a>
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/133277_NOV2021_HIGHEND_GIFT_PC_P02.jpg"
                                       data-lightbox-fifth="roadtrip">
                                        <img
                                            src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/133277_NOV2021_HIGHEND_GIFT_PC_P02.jpg"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>

                            <script src="/weekender_assets/js/modal_lightbox_fifth.min.js"></script>
                        </div>
                    @endif
                    @if($data->NovemberMYSTERYPC)
                        <div class="col-sm-4">
                            <div class="modal-wrap">
                                <h4 style="color: #EB1D32;text-align: center;">{{$data->NovemberMYSTERYPCLabel}}</h4>

                                <div class="slider-big">
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberMYSTERYPCResult1}}"
                                       data-lightbox-third="roadtrip">
                                        <center><img
                                                src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberMYSTERYPCResult1}}"
                                                style="width: 20%" alt=""></center>
                                    </a>
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/{{$data->NovemberMYSTERYPCResult2}}"
                                       data-lightbox-third="roadtrip">
                                        <img
                                            src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/{{$data->NovemberMYSTERYPCResult2}}"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>

                            <script src="/weekender_assets/js/modal_lightbox_third.min.js"></script>
                        </div>
                    @endif
                    @if($data->NovemberHighLimitPC)
                        <div class="col-sm-4">
                            <div class="modal-wrap">
                                <h4 style="color: #EB1D32;text-align: center;">{{$data->NovemberHighLimitPCLabel}}</h4>

                                <div class="slider-big">
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/134208_NOV2021_HIGH_LIMIT_P01.jpg"
                                       data-lightbox-fourth="roadtrip">
                                        <center><img
                                                src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/134208_NOV2021_HIGH_LIMIT_P01.jpg"
                                                style="width: 20%" alt=""></center>
                                    </a>
                                    <a href="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/hi_res/134208_NOV2021_HIGH_LIMIT_P02.jpg"
                                       data-lightbox-fourth="roadtrip">
                                        <img
                                            src="https://s3.amazonaws.com/shreveport.maplewebservices.com/november_2021/lo_res/134208_NOV2021_HIGH_LIMIT_P02.jpg"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>

                            <script src="/weekender_assets/js/modal_lightbox_fourth.min.js"></script>
                        </div>
                    @endif
                </div>
                <script src="/weekender_assets/js/modal_slick.min.js"></script>
                <script src="/weekender_assets/js/modal_main.js"></script>
            @else
                We do not have anything for you to see at this time, please check back later.
                @endif
                </br>
        </div>
        <div class="col-md-3">
            <a href="/" rel="nofollow">
                @switch($data->CSHRV_Tier)
                    @case('CHAIRMAN')

            <h5 style="font-weight: 600;color: #232325;text-align:center;">
                Welcome, {{ $data->first_name }} {{ $data->last_name}}</h5>
            <h5 style="font-weight: 600;color: white;text-align:center;background: #EB1D32;">
                Player Account: {{ $data->CSHRV_Account}}</h5>

            <h6 style="color:#EB1D32;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://shreveport.maplewebservices.com/admin/logout" style="color:#EB1D32">Logout</a></h6>
            <div id="TierLevel" class="textlineRed">Tier Level Chairman</div>

            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Points <span
                        style="color:#EB1D32;">{{ number_format($data->CSHRV_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Dollars <span
                        style="color:#EB1D32;">${{ $data->CSHRV_Comp_Dollars?$data->CSHRV_Comp_Dollars:0 }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Poker Rating <span
                        style="color:#EB1D32;">{{ $data-> CSHRV_Poker_Rating?$data-> CSHRV_Poker_Rating:0 }}</span>
                </div>
                <span
                    style="font-size: 12px;font-style: italic;color: darkgrey;"> Data updated at {{$data->updated_at}}</span>
            </div>
            <br>
            <hr style="height:1px;border:none;color:#EB1D32;background-color:#EB1D32;"/>
            @break
            @case('PRESIDENT')

            <h5 style="font-weight: 600;color: #232325;text-align:center;">
                Welcome, {{ $data->first_name }} {{ $data->last_name}}</h5>
            <h5 style="font-weight: 600;color: white;text-align:center;background: #EB1D32;">
                Player Account: {{ $data->CSHRV_Account}}</h5>
            <h6 style="color:#EB1D32;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://shreveport.maplewebservices.com/admin/logout" style="color:#EB1D32">Logout</a></h6>
            <div id="TierLevel" class="textlineBlack">Tier Level President</div>
            <hr style="height:20px;border:none;color:#EB1D32;background-color:black;"/>
            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Points <span
                        style="color:#EB1D32;">{{ number_format($data->CSHRV_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Dollars <span
                        style="color:#EB1D32;">${{ $data->CSHRV_Comp_Dollars?$data->CSHRV_Comp_Dollars:0 }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Poker Rating <span
                        style="color:#EB1D32;">{{ $data-> CSHRV_Poker_Rating?$data-> CSHRV_Poker_Rating:0 }}</span>
                </div>
                <span
                    style="font-size: 12px;font-style: italic;color: darkgrey;"> Data updated at {{$data->updated_at}}</span>
            </div>
            <br>
            <hr style="height:1px;border:none;color:#EB1D32;background-color:black;"/>
            @break
            @case('GOLD')

            <h5 style="font-weight: 600;color: #232325;text-align:center;">
                Welcome, {{ $data->first_name }} {{ $data->last_name}}</h5>
            <h5 style="font-weight: 600;color: white;text-align:center;background: #EB1D32;">
                Player Account: {{ $data->CSHRV_Account}}</h5>
            <h6 style="color:#EB1D32;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://shreveport.maplewebservices.com/admin/logout" style="color:#EB1D32">Logout</a></h6>
            <div id="TierLevel" class="textlinePlatinum">Tier Level Gold</div>

            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Points <span
                        style="color:#EB1D32;">{{ number_format($data->CSHRV_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Dollars <span
                        style="color:#EB1D32;">${{ $data->CSHRV_Comp_Dollars?$data->CSHRV_Comp_Dollars:0 }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Poker Rating <span
                        style="color:#EB1D32;">{{ $data-> CSHRV_Poker_Rating?$data-> CSHRV_Poker_Rating:0 }}</span>
                </div>
                <span
                    style="font-size: 12px;font-style: italic;color: darkgrey;"> Data updated at {{$data->updated_at}}</span>
            </div>
            <br>
            <hr style="height:1px;border:none;color:#EB1D32;background-color:#EB1D32;"/>
            @break
            @case('SILVER')

            <h5 style="font-weight: 600;color: #232325;text-align:center;">
                Welcome, {{ $data->first_name }} {{ $data->last_name}}</h5>
            <h5 style="font-weight: 600;color: white;text-align:center;background: #EB1D32;">
                Player Account: {{ $data->CSHRV_Account}}</h5>
            <h6 style="color:#EB1D32;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://shreveport.maplewebservices.com/admin/logout" style="color:#EB1D32">Logout</a></h6>
            <div id="TierLevel" class="textlinePlatinum">Tier Level Silver</div>
            <hr style="height:20px;border:none;color:#EB1D32;background-color:#e5e4e2;"/>
            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Points <span
                        style="color:#EB1D32;">{{ number_format($data->CSHRV_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Dollars <span
                        style="color:#EB1D32;">${{ $data->CSHRV_Comp_Dollars?$data->CSHRV_Comp_Dollars:0 }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Poker Rating <span
                        style="color:#EB1D32;">{{ $data-> CSHRV_Poker_Rating?$data-> CSHRV_Poker_Rating:0 }}</span>
                </div>
                <span
                    style="font-size: 12px;font-style: italic;color: darkgrey;"> Data updated at {{$data->updated_at}}</span>
            </div>
            <br>
            <hr style="height:1px;border:none;color:#EB1D32;background-color:#e5e4e2;"/>
            @break
            @default

            <h5 style="font-weight: 600;color: #232325;text-align:center;">
                Welcome, {{ $data->first_name }} {{ $data->last_name}}</h5>
            <h5 style="font-weight: 600;color: white;text-align:center;background: #EB1D32;">
                Player Account: {{ $data->CSHRV_Account}}</h5>
            <h6 style="color:#EB1D32;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://shreveport.maplewebservices.com/admin/logout" style="color:#EB1D32">Logout</a></h6>
            <div id="TierLevel" class="textlineGold">Tier Level Bronze</div>
            <hr style="height:20px;border:none;color:#EB1D32;background-color:#FFD700;"/>
            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Points <span
                        style="color:#EB1D32;">{{ number_format($data->CSHRV_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Dollars <span
                        style="color:#EB1D32;">${{ $data->CSHRV_Comp_Dollars?$data->CSHRV_Comp_Dollars:0 }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Poker Rating <span
                        style="color:#EB1D32;">{{ $data-> CSHRV_Poker_Rating?$data-> CSHRV_Poker_Rating:0 }}</span>
                </div>
                <span
                    style="font-size: 12px;font-style: italic;color: darkgrey;"> Data updated at {{$data->updated_at}}</span>
            </div>
            <br>
            <hr style="height:1px;border:none;color:#EB1D32;background-color:#FFD700;"/>
            @endswitch
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="/hostimage.jpg" alt="" style="max-width: 90px"></br>
                    <span style="color:black; margin:10px; font-size: 14px"><b>VIP Services</b></span></br>
                    <span style="color:black; margin:10px; font-size: 12px"><b>Office: <a href="tel:877-602-0711">877-602-0711</a></b></span>
                </div>
            </div>
        </div>
    </div>

@endsection
