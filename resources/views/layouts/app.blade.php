<!doctype html>

<html class="no-js" lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!--Google Fonts Below-->
    <link rel="stylesheet" href="{{asset('assets/css/phv1gon.css')}}">

    <!-- Icons & Favicons -->
    <link rel="icon" href="https://s3.amazonaws.com/shreveport.maplewebservices.com/logo/favicon.png">
    <link
        href="https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/images/apple-icon-touch.png"
        rel="apple-touch-icon"/>
    <!--[if IE]>
    <link rel="shortcut icon" href="https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/favicon.ico">
    <![endif]-->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage"
          content="https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/images/win8-tile-icon.png">
    <meta name="theme-color" content="#121212">

    <link rel="pingback" href="https://eldoradoshrev.wpengine.com/xmlrpc.php">

    <title>Bally's Shreveport</title>
    <meta name='robots' content='noindex, nofollow'/>

    <!-- The SEO Framework by Sybre Waaijer -->
    <meta name="description"
          content="Bally’s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&#8217;s AC today."/>
    <meta property="og:image"
          content="https://eldoradoshrev.wpengine.com/wp-content/uploads/2020/12/BAC_Logo_blk-scaled.jpg"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Eldorado Resort Casino Shreveport"/>
    <meta property="og:description"
          content="Bally’s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&#8217;s AC today."/>
    <meta property="og:url" content="https://eldoradoshrev.wpengine.com/"/>
    <meta property="og:site_name" content="Eldorado Resort Casino Shreveport"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:title" content="Eldorado Resort Casino Shreveport"/>
    <meta name="twitter:description"
          content="Bally’s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&#8217;s AC today."/>
    <meta name="twitter:image"
          content="https://eldoradoshrev.wpengine.com/wp-content/uploads/2020/12/BAC_Logo_blk-scaled.jpg"/>
    <link rel="canonical" href="https://eldoradoshrev.wpengine.com/"/>
    <script type="application/ld+json">
        {"@context":"https://schema.org","@type":"WebSite","url":"https://eldoradoshrev.wpengine.com/","name":"Eldorado Resort Casino Shreveport","potentialAction":{"@type":"SearchAction","target":"https://eldoradoshrev.wpengine.com/search/{search_term_string}/","query-input":"required name=search_term_string"}}


    </script>
    <script type="application/ld+json">
        {"@context":"https://schema.org","@type":"Organization","url":"https://eldoradoshrev.wpengine.com/","name":"Eldorado Resort Casino Shreveport","logo":"https://eldoradoshrev.wpengine.com/wp-content/uploads/2020/12/BAC_Logo_blk-scaled.jpg"}


    </script>
    <!-- / The SEO Framework by Sybre Waaijer | 0.12ms meta | 0.69ms boot -->
    <script type="text/javascript" src="{{asset('flipbook_assets/js/jquery.min.1.7.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel='dns-prefetch' href='https://s.w.org/'/>
    <link rel="alternate" type="application/rss+xml" title="Eldorado Resort Casino Shreveport &raquo; Feed"
          href="https://eldoradoshrev.wpengine.com/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Eldorado Resort Casino Shreveport &raquo; Comments Feed"
          href="https://eldoradoshrev.wpengine.com/comments/feed/"/>
    <script type="text/javascript"
            src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="dns-prefetch" href="https://maxcdn.bootstrapcdn.com/">
    <link rel='stylesheet' id='wp-block-library-css'
          href='https://eldoradoshrev.wpengine.com/wp-includes/css/dist/block-library/style.min.css?ver=5.7'
          type='text/css' media='all'/>

    <link rel='stylesheet' id='normalize-css-css'
          href='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/vendor/foundation/css/normalize.min.css?ver=5.7'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='foundation-css-css'
          href='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/vendor/foundation/css/foundation.min.css?ver=5.7'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='site-css-css'
          href='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/css/style.css?ver=5.7'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='font-awesome-css'
          href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?ver=5.7' type='text/css'
          media='all'/>
    <link rel="https://api.w.org/" href="https://eldoradoshrev.wpengine.com/wp-json/"/>
    <link rel="alternate" type="application/json" href="https://eldoradoshrev.wpengine.com/wp-json/wp/v2/pages/19"/>
    <link rel="alternate" type="application/json+oembed"
          href="https://eldoradoshrev.wpengine.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Feldoradoshrev.wpengine.com%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="https://eldoradoshrev.wpengine.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Feldoradoshrev.wpengine.com%2F&amp;format=xml"/>
    <link rel='stylesheet' id='font-awesome-css'
          media='all'/>
    <style type="text/css" id="wp-custom-css">
        .page-title {
            display: none;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('https://shreveport.maplewebservices.com/loading.gif') 50% 50% no-repeat rgb(255, 255, 255);
        }
        @media only screen and (max-width:660px) {
  
  .tab-bar {
    background: #EB1D32;
  }
  .menu-icon {
    height: 100%;
    width: 100%;
    background: #EB1D32;
  }
}
    </style>
    <link href="/weekender_assets/css/modal_slick.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_slick-theme.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_lightbox.min.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_style.css" rel="stylesheet">
</head>

<body class="home page-template-default page page-id-19">
<div id="flipbook-section">
    <div id="flipbook-section1">
        <div id="flipbook-section2">
            <div id="weekender-section">
                <aside role="alert" class="alert-banner" style="display: none;">
                    <div class="pagewidth">
                    </div>
                </aside>
                <div class="off-canvas-wrap" data-offcanvas>
                    <div class="inner-wrap">
                        <div id="container">
                            <header class="header" role="banner" style="background-color: #EB1D32;">
                                <div id="inner-header" class="row clearfix">
                                    <div class="show-for-medium-up contain-to-grid">
                                        <div class="col-sm-3"
                                             style="display: flex;justify-content: center;align-items: center;">
                                            <!-- Title Area -->
                                            <a href="https://eldoradoshrev.wpengine.com/" rel="nofollow">
                                                <!-- Eldorado Resort Casino Shreveport</a> -->
                                                <img
                                                    src="https://s3.amazonaws.com/shreveport.maplewebservices.com/logo/ballys_kc_main-logo.png"
                                                    alt="Eldorado Shreveport logo" width="100%"></a>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div id="book-a-room" style="display: none;">
                                                        <a href="https://eldoradoshreveport.reztrip.com/"
                                                           target="_blank" rel="noopener">
                                                            <button>Book a Room</button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <section class="top-bar-section">
                                                        <ul id="menu-main-navigation" class="top-bar-menu right"
                                                            style="list-style: none;     margin-top: 7%; color:#111;">
                                                            <li class="divider"></li>
                                                            <li id="menu-item-5037" 
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5037">
                                                                <a href="https://eldoradoshrev.wpengine.com/gaming/" style="color: #EB1D32;">Gaming</a>
                                                                <ul class="sub-menu dropdown">
                                                                    <li id="menu-item-5293"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5293">
                                                                        <a href="https://eldoradoshrev.wpengine.com/player-portal/" style="color: #EB1D32;">Player
                                                                            Portal</a></li>
                                                                    <li id="menu-item-5049"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5049">
                                                                        <a href="https://eldoradoshrev.wpengine.com/gaming/promotions-and-tournaments/" style="color: #EB1D32;">Promotions
                                                                            and Tournaments</a></li>
                                                                    <li id="menu-item-5050"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5050">
                                                                        <a href="https://eldoradoshrev.wpengine.com/gaming/gaming-overview/" style="color: #EB1D32;">Gaming
                                                                            Overview</a></li>
                                                                    <li id="menu-item-5295"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5295">
                                                                        <a href="https://eldoradoshrev.wpengine.com/gaming/slots/" style="color: #EB1D32;">Slots</a>
                                                                    </li>
                                                                    <li id="menu-item-5296"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5296">
                                                                        <a href="https://eldoradoshrev.wpengine.com/gaming/table-games/" style="color: #EB1D32;">Table
                                                                            Games</a></li>
                                                                    <li id="menu-item-5305"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5305">
                                                                        <a href="https://eldoradoshrev.wpengine.com/gaming/poker/" style="color: #EB1D32;">Poker</a>
                                                                    </li>
                                                                    <li id="menu-item-5297"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5297">
                                                                        <a href="https://eldoradoshrev.wpengine.com/gaming/jackpot-winners/" style="color: #EB1D32;">Jackpot
                                                                            Winners</a></li>
                                                                    <li id="menu-item-5048"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5048">
                                                                        <a href="https://eldoradoshrev.wpengine.com/gaming/services/" style="color: #EB1D32;">Services</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li id="menu-item-5031"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5031">
                                                                <a href="https://eldoradoshrev.wpengine.com/rooms-suites/" style="color: #EB1D32;">Rooms
                                                                    &#038;
                                                                    Suites</a>
                                                                <ul class="sub-menu dropdown">
                                                                    <li id="menu-item-5186"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5186">
                                                                        <a href="https://eldoradoshrev.wpengine.com/rooms-suites/premium-king/" style="color: #EB1D32;">Premium
                                                                            King</a></li>
                                                                    <li id="menu-item-5187"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5187">
                                                                        <a href="https://eldoradoshrev.wpengine.com/rooms-suites/premium-queen/" style="color: #EB1D32;">Premium
                                                                            Queen</a></li>
                                                                    <li id="menu-item-5185"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5185">
                                                                        <a href="https://eldoradoshrev.wpengine.com/rooms-suites/monarch-suite/" style="color: #EB1D32;">Monarch
                                                                            Suite</a></li>
                                                                    <li id="menu-item-5188"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5188">
                                                                        <a href="https://eldoradoshrev.wpengine.com/rooms-suites/silver-suite-king-2/" style="color: #EB1D32;">Silver
                                                                            Suite King</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li id="menu-item-4845"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4845">
                                                                <a href="https://eldoradoshrev.wpengine.com/dining/" style="color: #EB1D32;">Dining</a>
                                                                <ul class="sub-menu dropdown">
                                                                    <li id="menu-item-5064"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5064">
                                                                        <a href="https://eldoradoshrev.wpengine.com/dining/the-vintage/" style="color: #EB1D32;">The
                                                                            Vintage</a></li>
                                                                    <li id="menu-item-5063"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5063">
                                                                        <a href="https://eldoradoshrev.wpengine.com/dining/sportsmans-paradise-cafe/" style="color: #EB1D32;">Sportsman’s
                                                                            Paradise Cafe</a></li>
                                                                    <li id="menu-item-5061"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5061">
                                                                        <a href="https://eldoradoshrev.wpengine.com/dining/noodle-bar/" style="color: #EB1D32;">Noodle
                                                                            Bar</a></li>
                                                                    <li id="menu-item-5263"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5263">
                                                                        <a href="https://eldoradoshrev.wpengine.com/sportsmans-express/" style="color: #EB1D32;">Sportsman’s
                                                                            Express</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li id="menu-item-5039"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5039">
                                                                <a href="https://eldoradoshrev.wpengine.com/entertainment/" style="color: #EB1D32;">Entertainment</a>
                                                                <ul class="sub-menu dropdown">
                                                                    <li id="menu-item-5133"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5133">
                                                                        <a href="https://eldoradoshrev.wpengine.com/entertainment/celebrity-lounge/" style="color: #EB1D32;">Celebrity
                                                                            Lounge</a></li>
                                                                    <li id="menu-item-5131"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5131">
                                                                        <a href="https://eldoradoshrev.wpengine.com/entertainment/allure-ultra-lounge/" style="color: #EB1D32;">Allure
                                                                            Ultra Lounge</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li id="menu-item-5291"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5291">
                                                                <a href="https://eldoradoshrev.wpengine.com/contact/" style="color: #EB1D32;">Contact</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li id="menu-item-5290"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-5290">
                                                                <a href="#" style="color: #EB1D32;">More</a>
                                                                <ul class="sub-menu dropdown">
                                                                    <li id="menu-item-5026"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5026">
                                                                        <a href="https://eldoradoshrev.wpengine.com/offers/" style="color: #EB1D32;">Offers</a>
                                                                    </li>
                                                                    <li id="menu-item-5035"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5035">
                                                                        <a href="https://eldoradoshrev.wpengine.com/meetings/" style="color: #EB1D32;">Meetings</a>
                                                                    </li>
                                                                    <li id="menu-item-5036"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5036">
                                                                        <a href="https://eldoradoshrev.wpengine.com/amenities/" style="color: #EB1D32;">Amenities</a>
                                                                        <ul class="sub-menu dropdown">
                                                                            <li id="menu-item-5182"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5182">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/la-spa/" style="color: #EB1D32;">LA
                                                                                    Spa</a></li>
                                                                            <li id="menu-item-5180"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5180">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/gift-shop/" style="color: #EB1D32;">Gift
                                                                                    Shop</a></li>
                                                                            <li id="menu-item-5178"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5178">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/canine-friendly-rooms/" style="color: #EB1D32;">Canine
                                                                                    Friendly Rooms</a></li>
                                                                            <li id="menu-item-5179"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5179">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/fitness-center/" style="color: #EB1D32;">Fitness
                                                                                    Center</a></li>
                                                                            <li id="menu-item-5304"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5304">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/wireless-internet-access/" style="color: #EB1D32;">Wireless
                                                                                    Internet Access</a></li>
                                                                            <li id="menu-item-5184"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5184">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/room-service/" style="color: #EB1D32;">Room
                                                                                    Service</a></li>
                                                                            <li id="menu-item-5181"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5181">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/guest-services/" style="color: #EB1D32;">Guest
                                                                                    Services</a></li>
                                                                            <li id="menu-item-5183"
                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5183">
                                                                                <a href="https://eldoradoshrev.wpengine.com/amenities/parking/" style="color: #EB1D32;">Parking</a>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            @if ((Auth::User()))
                                                                <li id="menu-item-5012" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011">
                                                                    <a href="{{ route('voyager.logout') }}" style="color: #EB1D32;">Logout</a>
                                                                </li>
                                                                <form id="logout-form"
                                                                      action="{{ route('voyager.logout') }}"
                                                                      method="POST"
                                                                      style="display: none;">
                                                                    @csrf
                                                                </form>
                                                            @endif
                                                        </ul>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sticky fixed show-for-small">
                                        <nav class="tab-bar" style="    height: 50px;width: 100%;">
                                            <section class="middle tab-bar-section">
                                                <a href="https://eldoradoshrev.wpengine.com/" rel="nofollow"
                                                   style="text-decoration: none;" style="color: #EB1D32 !important;"><h1 class="title">Bally's Shreveport</h1></a>
                                            </section>
                                            <section class="left-small">
                                                <a href="#" class="left-off-canvas-toggle menu-icon"><span><i
                                                            class="fa fab fa-bars"></i></span></a>
                                            </section>
                                        </nav>
                                    </div>

                                    <aside class="left-off-canvas-menu show-for-small-only">
                                        <div id="book-a-room">
                                            <button><a href="https://eldoradoshreveport.reztrip.com/" style="color: #EB1D32;">Book a Room</a>
                                            </button>
                                        </div>
                                        <ul class="off-canvas-list">
                                            <!-- <li><label>Navigation</label></li> -->
                                            <a href="https://eldoradoshrev.wpengine.com/"><span
                                                    class="home-link" style="color: #EB1D32;">Home</span></a>

                                            <ul id="menu-main-navigation-1" class="off-canvas-list">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5037">
                                                    <a href="https://eldoradoshrev.wpengine.com/gaming/" style="color: #EB1D32;">Gaming</a>
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5293">
                                                            <a href="https://eldoradoshrev.wpengine.com/player-portal/"style="color: #EB1D32;">Player
                                                                Portal</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5049">
                                                            <a href="https://eldoradoshrev.wpengine.com/gaming/promotions-and-tournaments/" style="color: #EB1D32;">Promotions
                                                                and Tournaments</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5050">
                                                            <a href="https://eldoradoshrev.wpengine.com/gaming/gaming-overview/">Gaming
                                                                Overview</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5295">
                                                            <a href="https://eldoradoshrev.wpengine.com/gaming/slots/">Slots</a>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5296">
                                                            <a href="https://eldoradoshrev.wpengine.com/gaming/table-games/">Table
                                                                Games</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5305">
                                                            <a href="https://eldoradoshrev.wpengine.com/gaming/poker/">Poker</a>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5297">
                                                            <a href="https://eldoradoshrev.wpengine.com/gaming/jackpot-winners/">Jackpot
                                                                Winners</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5048">
                                                            <a href="https://eldoradoshrev.wpengine.com/gaming/services/">Services</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5031">
                                                    <a href="https://eldoradoshrev.wpengine.com/rooms-suites/">Rooms
                                                        &#038; Suites</a>
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5186">
                                                            <a href="https://eldoradoshrev.wpengine.com/rooms-suites/premium-king/">Premium
                                                                King</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5187">
                                                            <a href="https://eldoradoshrev.wpengine.com/rooms-suites/premium-queen/">Premium
                                                                Queen</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5185">
                                                            <a href="https://eldoradoshrev.wpengine.com/rooms-suites/monarch-suite/">Monarch
                                                                Suite</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5188">
                                                            <a href="https://eldoradoshrev.wpengine.com/rooms-suites/silver-suite-king-2/">Silver
                                                                Suite King</a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4845">
                                                    <a href="https://eldoradoshrev.wpengine.com/dining/">Dining</a>
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5064">
                                                            <a href="https://eldoradoshrev.wpengine.com/dining/the-vintage/">The
                                                                Vintage</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5063">
                                                            <a href="https://eldoradoshrev.wpengine.com/dining/sportsmans-paradise-cafe/">Sportsman’s
                                                                Paradise Cafe</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5061">
                                                            <a href="https://eldoradoshrev.wpengine.com/dining/noodle-bar/">Noodle
                                                                Bar</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5263">
                                                            <a href="https://eldoradoshrev.wpengine.com/sportsmans-express/">Sportsman’s
                                                                Express</a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5039">
                                                    <a href="https://eldoradoshrev.wpengine.com/entertainment/">Entertainment</a>
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5133">
                                                            <a href="https://eldoradoshrev.wpengine.com/entertainment/celebrity-lounge/">Celebrity
                                                                Lounge</a></li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5131">
                                                            <a href="https://eldoradoshrev.wpengine.com/entertainment/allure-ultra-lounge/">Allure
                                                                Ultra Lounge</a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5291">
                                                    <a
                                                        href="https://eldoradoshrev.wpengine.com/contact/" style="color: #EB1D32;">Contact</a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-5290">
                                                    <a href="#" style="color: #EB1D32;">More</a>
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5026">
                                                            <a href="https://eldoradoshrev.wpengine.com/offers/" style="color: #EB1D32;">Offers</a>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5035">
                                                            <a href="https://eldoradoshrev.wpengine.com/meetings/" style="color: #EB1D32;">Meetings</a>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5036">
                                                            <a href="https://eldoradoshrev.wpengine.com/amenities/" style="color: #EB1D32;">Amenities</a>
                                                            <ul class="sub-menu">
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5182">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/la-spa/" style="color: #EB1D32;">LA
                                                                        Spa</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5180">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/gift-shop/" style="color: #EB1D32;">Gift
                                                                        Shop</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5178">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/canine-friendly-rooms/" style="color: #EB1D32;">Canine
                                                                        Friendly Rooms</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5179">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/fitness-center/" style="color: #EB1D32;">Fitness
                                                                        Center</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5304">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/wireless-internet-access/" style="color: #EB1D32;">Wireless
                                                                        Internet Access</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5184">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/room-service/" style="color: #EB1D32;">Room
                                                                        Service</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5181">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/guest-services/" style="color: #EB1D32;">Guest
                                                                        Services</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5183">
                                                                    <a href="https://eldoradoshrev.wpengine.com/amenities/parking/" style="color: #EB1D32;">Parking</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </ul>
                                    </aside>

                                    <a class="exit-off-canvas"></a></div>  <!-- end #inner-header -->
                            </header> <!-- end .header -->
                            <div class="loader"></div>
                            <div id="content" style="background-color: #EB1D32;">
                                @yield('content')
                            </div>
                            <footer class="footer" role="contentinfo" style="background-color: #EB1D32;">
						<div id="inner-footer" class="row clearfix fullwidth" style="background-color: #EB1D32;">
							<div class="large-12 medium-12 columns">
								<div id="execphp-14" class="widget-odd widget-first widget-1 widget widget_execphp">			<div class="execphpwidget"><p>Bally's Shreveport<br>
451 Clyde Fant Parkway, Shreveport, LA 71101</p>

<p>Local: <a href="tel:318-220-0711">(318) 220-0711</a><br> Toll-Free: <a href="tel:877-602-0711">(877) 602-0711</a><br>
Hotel Reservations: <a href="tel:877-613-0711">(877) 613-0711</a></p>

<a href="https://twitter.com/ballysshrvprt"><span class="fa fa-twitter-square" title="Twitter" target="_blank"></span><span class="screen-reader-text">Like Us on Facebook</span></a>

<a href="https://www.facebook.com/ballysshreveport"><span class="fa fa-facebook-square" title="Twitter" target="_blank"></span><span class="screen-reader-text">Follow Us on Twitter</span></a>

<a href="https://www.instagram.com/ballysshreveport/"><span class="fa fa-instagram" title="instagram" target="_blank"></span><span class="screen-reader-text">Follow Us on Instagram</span></a>

<!--- <a href="https://www.youtube.com/channel/UCaJU7h_Kl0Wb33eudKZXasQ"><span class="fa fa-youtube" title="youtube" target="_blank"></span><span class="screen-reader-text">Watch Us on Youtube</span></a> -->

<style>a span.screen-reader-text {
  clip: rect(1px, 1px, 1px, 1px);
    position: absolute !important;
    height: 1px;
    width: 1px;
    overflow: hidden;
    color:#fff;
font-size:20px;
}</style>
</div>
		</div><div id="nav_menu-3" class="widget-even widget-2 widget widget_nav_menu"><h2 class="widgettitle">Additional Links</h2><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-5197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5197"><a href="offers.html">Offers</a></li>
<li id="menu-item-5198" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5198"><a href="rooms-suites.html">Rooms &amp; Suites</a></li>
<li id="menu-item-5212" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5212"><a href="slots.html">Slots</a></li>
<li id="menu-item-5213" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5213"><a href="table-games.html">Table Games</a></li>
<li id="menu-item-5225" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5225"><a href="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=02DE7651BB6513B8F4759F0BC3EF6052">Employment Opportunities</a></li>
<li id="menu-item-5230" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5230"><a href="https://eldoradoshreveport.reztrip.com/classic/en/searches/605be570d701b301385811df/hotels/SRP/rooms?action=show&amp;controller=landings#?category=">View / Cancel Reservations</a></li>
<li id="menu-item-5236" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5236"><a href="frequently-asked-questions.html">Frequently Asked Questions</a></li>
<li id="menu-item-5237" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5237"><a href="about.html">About</a></li>
<li id="menu-item-5238" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5238"><a href="awards-and-accolades.html">Awards and Accolades</a></li>
<li id="menu-item-5239" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5239"><a href="lost-and-found.html">Lost and Found</a></li>
<li id="menu-item-5240" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5240"><a href="accessibility-statement.html">Accessibility Statement</a></li>
<li id="menu-item-5241" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5241"><a href="vendors.html">Vendors</a></li>
</ul></div></div><div id="execphp-15" class="widget-odd widget-last widget-3 join-our-mailing-list-widget widget widget_execphp">			<div class="execphpwidget"><a href="https://www.ballys.com/casinos-and-resorts.htm" target="_blank" rel="noopener"><img src="https://s3.amazonaws.com/shreveport.maplewebservices.com/logo/bly_lg_1cp_wht_rev_210420.png" width="60%" alt="Bally's Corporation"> </a></div>
		</div>								<div style="clear: both"></div>					
								<div id="execphp-16" class="widget-odd widget-first widget-1 post-footer widget_execphp center">			<div class="execphpwidget"><p>Copyright &copy;2021 Bally's Shreveport.<br> All rights reserved. | <a href="privacy-policy.html">Privacy Policy</a> | <a href="cookie-policy.html">Cookie Policy</a></p>

<p style="color: #111;"><b>GAMBLING PROBLEM? CALL <a style="color: #111;" href="tel:877-770-7867">1-877-770-7867</a></b></p></div>
		</div><div id="execphp-18" class="widget-even widget-last widget-2 widget_execphp center">			<div class="execphpwidget"><hr>

<p style="font-weight: 300; font-size: 0.75rem;">An inherent risk of exposure to COVID-19 exists in any public place where people are present. COVID-19 is an extremely contagious disease that can lead to severe illness and even death. According to the <a style="font-weight: 300; font-size: 0.75rem;" href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html">Centers for Disease Control and Prevention,</a> senior citizens and guests with underlying medical conditions are especially vulnerable. By visiting Bally's Shreveport you voluntarily assume all risks related to exposure to COVID-19.</p></div>
		</div>					
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
                        </div> <!-- end #container -->
                    </div> <!-- end .inner-wrap -->
                </div> <!-- end .off-canvas-wrap -->
                <link rel='stylesheet' id='soliloquy-lite-style-css'
                      href='https://eldoradoshrev.wpengine.com/wp-content/plugins/soliloquy-lite/assets/css/soliloquy.css?ver=2.6.1'
                      type='text/css' media='all'/>
                <script type='text/javascript'
                        src='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/vendor/foundation/js/vendor/modernizr.js?ver=2.8.3'
                        id='modernizr-js'></script>
                <script type='text/javascript'
                        src='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/vendor/foundation/js/foundation.min.js?ver=5.7'
                        id='foundation-js-js'></script>
                <script type='text/javascript'
                        src='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/js/scripts.js?ver=5.7'
                        id='site-js-js'></script>
                <script type='text/javascript'
                        src='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/js/jquery.bxslider.js?ver=5.7'
                        id='bx-slider-js'></script>
                <script type='text/javascript'
                        src='https://eldoradoshrev.wpengine.com/wp-content/themes/EldoradoShrevport/assets/js/isotope.pkgd.min.js?ver=5.7'
                        id='isotope-js'></script>
                <script type='text/javascript'
                        src='https://eldoradoshrev.wpengine.com/wp-includes/js/wp-embed.min.js?ver=5.7'
                        id='wp-embed-js'></script>
                <script type='text/javascript'
                        src='https://eldoradoshrev.wpengine.com/wp-content/plugins/soliloquy-lite/assets/js/min/soliloquy-min.js?ver=2.6.1'
                        id='soliloquy-lite-script-js'></script>
                <script type="text/javascript">
                    if (typeof soliloquy_slider === 'undefined' || false === soliloquy_slider) {
                        soliloquy_slider = {};
                    }
                    jQuery('#soliloquy-container-20').css('height', Math.round(jQuery('#soliloquy-container-20').width() / (1185 / 545)));
                    jQuery(window).load(function () {
                        var $ = jQuery;
                        var soliloquy_container_20 = $('#soliloquy-container-20'), soliloquy_20 = $('#soliloquy-20');
                        soliloquy_slider['20'] = soliloquy_20.soliloquy({
                            slideSelector: '.soliloquy-item',
                            speed: 650,
                            pause: 6000,
                            auto: 1,
                            useCSS: 0,
                            keyboard: true,
                            adaptiveHeight: 1,
                            adaptiveHeightSpeed: 400,
                            infiniteLoop: 1,
                            mode: 'fade',
                            pager: 1,
                            controls: 1,
                            nextText: '',
                            prevText: '',
                            startText: '',
                            stopText: '',
                            onSliderLoad: function (currentIndex) {
                                soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden', 'true');
                                soliloquy_container_20.css({'height': 'auto', 'background-image': 'none'});
                                if (soliloquy_container_20.find('.soliloquy-slider li').length > 1) {
                                    soliloquy_container_20.find('.soliloquy-controls').fadeTo(300, 1);
                                }
                                soliloquy_20.find('.soliloquy-item:not(.soliloquy-clone):eq(' + currentIndex + ')').addClass('soliloquy-active-slide').attr('aria-hidden', 'false');
                                soliloquy_container_20.find('.soliloquy-clone').find('*').removeAttr('id');
                                soliloquy_container_20.find('.soliloquy-controls-direction').attr('aria-label', 'carousel buttons').attr('aria-controls', 'soliloquy-container-20');
                                soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-prev').attr('aria-label', 'previous');
                                soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-next').attr('aria-label', 'next');
                            },
                            onSlideBefore: function (element, oldIndex, newIndex) {
                                soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden', 'true');
                                $(element).addClass('soliloquy-active-slide').attr('aria-hidden', 'false');
                            },
                            onSlideAfter: function (element, oldIndex, newIndex) {
                            },
                        });
                    });            </script>
            </div>
        </div>
    </div>
</div>
</body>
<footer>
    <script>
        $(window).load(function () {
            $(".loader").fadeOut("slow");
        });
    </script>
</footer>
</html> <!-- end page -->

